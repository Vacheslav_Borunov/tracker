// @ts-nocheck
import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react'
import { Routes, Route } from 'react-router-dom'
import Home from './pages/Home/Home';
import HatBar from './components/HatBar';
import FooterBar from './components/FooterBar';
import Managers from './pages/Managers/Managers';
import CreateTask from './pages/CreateTask/CreateTask';
import NotfoundPage from './pages/NotfoundPage/NotfoundPage';
import UserPage from './pages/UserPage/UserPage';
import TaskPage from './pages/TaskPage/TaskPage';
import Testers from './pages/Testers/Testers';

import styles from './app.module.css';

const App = observer(() => {
  const [ testers, setTesters ] = useState([])

  const [ managers, setManagers ] = useState([])

  const [ createdTasks, setСreatedTasks ] = useState<Props>([])

  const [ workTasks, setWorkTasks ] = useState([])

  const [ completedTasks, setCompletedTasks ] = useState([])

  const [ taskInfo, setTaskInfo ] = useState({})

  useEffect(() => {
    if (localStorage.getItem('testers') !== null) {
      const localTesters = JSON.parse(localStorage.getItem('testers'))
      setTesters(localTesters)
    }
    if (localStorage.getItem('managers') !== null) {
      const localManagers = JSON.parse(localStorage.getItem('managers'))
      setManagers(localManagers)
    }
    if (localStorage.getItem('createdTasks') !== null) {
      const localCreatedTasks = JSON.parse(localStorage.getItem('createdTasks'))
      setСreatedTasks(localCreatedTasks)
    }
    if (localStorage.getItem('workTasks') !== null) {
      const localWorkTasks = JSON.parse(localStorage.getItem('workTasks'))
      setWorkTasks(localWorkTasks)
    }
    if (localStorage.getItem('completedTasks') !== null) {
      const localCompletedTasks = JSON.parse(localStorage.getItem('completedTasks'))
      setCompletedTasks(localCompletedTasks)
    }
  }, [])

  return (
    <div className={ styles.fon }>
      <HatBar />
      <Routes>
        <Route
          path="/"
          element={ (
            <Home
              setTaskInfo={ setTaskInfo }
              createdTasks={ createdTasks }
              setСreatedTasks={ setСreatedTasks }
              workTasks={ workTasks }
              setWorkTasks={ setWorkTasks }
              completedTasks={ completedTasks }
              setCompletedTasks={ setCompletedTasks }
            />
        ) }
        />
        <Route
          path="/createTask"
          element={ (
            <CreateTask
              setСreatedTasks={ setСreatedTasks }
              createdTasks={ createdTasks }
              testers={ testers }
            />
        ) }
        />
        <Route
          path="/managers"
          element={ (
            <Managers
              managers={ managers }
              setManagers={ setManagers }
            />
        ) }
        />
        <Route
          path="/testers"
          element={ (
            <Testers
              testers={ testers }
              setTesters={ setTesters }
            />
        ) }
        />
        <Route
          path="/userPage"
          element={ (
            <UserPage />
        ) }
        />
        <Route
          path="/taskPage"
          element={ (
            <TaskPage
              taskInfo={ taskInfo }
              testers={ testers }
              createdTasks={ createdTasks }
              setСreatedTasks={ setСreatedTasks }
              workTasks={ workTasks }
              setWorkTasks={ setWorkTasks }
              completedTasks={ completedTasks }
              setCompletedTasks={ setCompletedTasks }
            />
        ) }
        />
        <Route path="*" element={ <NotfoundPage /> } />
      </Routes>
      <FooterBar />
    </div>
  )
})

export default App;
