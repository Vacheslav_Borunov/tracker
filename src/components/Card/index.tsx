import React, { FC, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { observer } from 'mobx-react'
import { Button } from '@chakra-ui/react';
import styles from './styles/index.module.css'

interface Props {
  time: number,
  taskName: string,
  taskDescription: string,
  taskTester: string,
  type: number,
  testCheck: number,
  taskComment: string,
  setTaskInfo: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }) => void,
  createdTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  workTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  completedTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  setСreatedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setWorkTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setCompletedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
}

const Card: FC<Props> = observer(({
  time,
  taskName,
  taskDescription,
  taskTester,
  type,
  testCheck,
  taskComment,
  setTaskInfo,
  createdTasks,
  workTasks,
  completedTasks,
  setСreatedTasks,
  setWorkTasks,
  setCompletedTasks,
}) => {
  const date = new Date(time)
  const dateString = date.toLocaleDateString()

  const openCard = () => {
    setTaskInfo({
      time, taskName, taskDescription, taskTester, type, testCheck, taskComment,
    })
  }

  const deleteCard = () => {
    if (createdTasks.find(el => el.time === time)) {
      const newСreatedTasks = createdTasks.filter(el => el.time !== time)

      setСreatedTasks(newСreatedTasks)
      localStorage.setItem('createdTasks', JSON.stringify(newСreatedTasks))
    } else if (workTasks.find(el => el.time === time)) {
      const newWorkTasks = workTasks.filter(el => el.time !== time)

      setWorkTasks(newWorkTasks)
      localStorage.setItem('workTasks', JSON.stringify(newWorkTasks))
    } else if (completedTasks.find(el => el.time === time)) {
      const newCompletedTasks = completedTasks.filter(el => el.time !== time)

      setCompletedTasks(newCompletedTasks)
      localStorage.setItem('completedTasks', JSON.stringify(newCompletedTasks))
    }
  }

  return (
    <div className={ styles.card }>
      <div className={ styles.wrapper_taskName }>
        <strong className={ styles.taskName }>{taskName}</strong>
      </div>
      <div>
        <strong>Тестировщик:</strong>
        <p className={ styles.taskTester }>{taskTester}</p>
      </div>
      <div>
        <strong>Дата создания:</strong>
        <p className={ styles.time }>{dateString}</p>
      </div>
      <div className={ styles.wrapper_btn }>
        <Link to="/taskPage">
          <Button size="sm" colorScheme="blue" onClick={ () => openCard() }>Подробнее</Button>
        </Link>
        <Button size="sm" colorScheme="red" onClick={ () => deleteCard() }>Удалить</Button>
      </div>
    </div>
  )
})

export default Card;
