import React from 'react';
import logoApp from '../../image/logo_app.png'

import styles from './styles/index.module.css'

const FooterBar = () => (
  <footer className={ styles.wrapper_footer }>
    <div className={ styles.wrapper_logo }>
      <img alt="logo_app" src={ logoApp } className={ styles.logo_app } />
      <p className={ styles.label_app }>Task Bord</p>
    </div>
  </footer>
)

export default FooterBar;
