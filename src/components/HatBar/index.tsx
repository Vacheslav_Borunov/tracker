/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { FC, useCallback } from 'react';
import { Link } from 'react-router-dom'
import logoApp from '../../image/logo_app.png'
import iconTester from '../../image/icon_tester.png'

import styles from './styles/index.module.css'

const HatBar: FC = () => (
  <header className={ styles.wrapper_header }>
    <Link to="/" className={ styles.wrapper_logo }>
      <img alt="logo_app" src={ logoApp } className={ styles.logo_app } />
      <p className={ styles.label_app }>Task Bord</p>
    </Link>
    <div className={ styles.wrapper_nav_search }>
      <nav className={ styles.wrapper_nav }>
        <ul>
          <li><Link to="/createTask">Создать задачу</Link></li>
          <li><Link to="/testers">Тестировщики</Link></li>
          <li><Link to="/managers">Руководители</Link></li>
        </ul>
      </nav>
    </div>
    <div className={ styles.wrapper_infoUser }>
      <Link to="/userPage">
        <img alt="iconUser" src={ iconTester } className={ styles.icon_User } />
      </Link>
    </div>
  </header>
)

export default HatBar;
