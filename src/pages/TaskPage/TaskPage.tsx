import React, { FC, useEffect, useState } from 'react';
import PropTypes, { bool, number } from 'prop-types';
import { observer } from 'mobx-react'
import {
  Button, Input, Select, Textarea,
  Checkbox,
  AlertIcon,
  AlertDescription,
  AlertTitle,
  Alert,
} from '@chakra-ui/react';
import styles from './styles/index.module.css'

interface Props {
  testers:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberTester: number,
    post: string,
  }[],
  taskInfo:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  },
  createdTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  workTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  completedTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  setСreatedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setWorkTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setCompletedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  }

const TaskPage: FC<Props> = observer(({
  taskInfo: {
    time,
    taskName,
    taskDescription,
    taskTester,
    type,
    testCheck,
    taskComment,
  },
  testers,
  createdTasks,
  workTasks,
  completedTasks,
  setСreatedTasks,
  setWorkTasks,
  setCompletedTasks,
}) => {
  const [ taskEdit, setTaskEdit ] = useState<boolean>(false)
  const [ newTaskName, setNewTaskName ] = useState<string>(taskName)
  const [ newTaskDescription, setNewTaskDescription ] = useState<string>(taskDescription)
  const [ newTaskTester, setNewTaskTester ] = useState<string>(taskTester)
  const [ newTestCheck, setNewTestCheck ] = useState<number>(testCheck)
  const [ newTaskComment, setNewTaskComment ] = useState<string>(taskComment)
  const [ flagAlarmError, setFlagAlarmError ] = useState<boolean>(false)

  let status = ''

  if (type === 1) {
    status = 'Создана'
  }
  if (type === 2) {
    status = 'В работе'
  }
  if (type === 3) {
    status = 'Готова'
  }

  const date = new Date(time)
  const dateString = date.toLocaleDateString()

  const handleTaskWork = () => {
    setWorkTasks([{
      time,
      taskName,
      taskDescription,
      taskTester,
      type: 2,
      testCheck: newTestCheck,
      taskComment: newTaskComment,
    }])
    localStorage.setItem('workTasks', JSON.stringify([ ...workTasks, {
      time,
      taskName,
      taskDescription,
      taskTester,
      type: 2,
      testCheck: newTestCheck,
      taskComment: newTaskComment,
    }]))

    const newCreatedTasks = createdTasks.filter(el => el.time !== time)
    setСreatedTasks(newCreatedTasks)
    localStorage.setItem('createdTasks', JSON.stringify(newCreatedTasks))
  }

  const handleCompletedTasks = () => {
    setCompletedTasks([{
      time,
      taskName,
      taskDescription,
      taskTester,
      type: 3,
      testCheck: newTestCheck,
      taskComment: newTaskComment,
    }])
    localStorage.setItem('completedTasks', JSON.stringify([ ...completedTasks, {
      time,
      taskName,
      taskDescription,
      taskTester,
      type: 3,
      testCheck: newTestCheck,
      taskComment: newTaskComment,
    }]))

    const newWorkTasks = workTasks.filter(el => el.time !== time)
    setWorkTasks(newWorkTasks)
    localStorage.setItem('workTasks', JSON.stringify(newWorkTasks))
  }

  const setTaskEditSave = () => {
    if (!newTaskName || !newTaskDescription) {
      setFlagAlarmError(true)
    } else {
      const idxCreatedTasks = createdTasks.findIndex(el => el.time === time)
      const idxWorkTasks = workTasks.findIndex(el => el.time === time)
      const idxCompletedTasks = completedTasks.findIndex(el => el.time === time)

      if (idxCreatedTasks >= 0) {
        const newСreatedTasks = createdTasks.slice()
        newСreatedTasks[idxCreatedTasks] = {
          time,
          taskName: newTaskName,
          taskDescription: newTaskDescription,
          taskTester: newTaskTester,
          type,
          testCheck: newTestCheck,
          taskComment: newTaskComment,
        }

        setСreatedTasks(newСreatedTasks)
        localStorage.setItem('createdTasks', JSON.stringify(newСreatedTasks))
        setTaskEdit(false)
        setFlagAlarmError(false)
      } else if (idxWorkTasks >= 0) {
        const newWorkTasks = workTasks.slice()

        newWorkTasks[idxWorkTasks] = {
          time,
          taskName: newTaskName,
          taskDescription: newTaskDescription,
          taskTester: newTaskTester,
          type,
          testCheck: newTestCheck,
          taskComment: newTaskComment,
        }
        setWorkTasks(newWorkTasks)
        localStorage.setItem('workTasks', JSON.stringify(newWorkTasks))

        setTaskEdit(false)
        setFlagAlarmError(false)
      } else if (idxCompletedTasks >= 0) {
        const newCompletedTasks = completedTasks.slice()

        newCompletedTasks[idxCompletedTasks] = {
          time,
          taskName: newTaskName,
          taskDescription: newTaskDescription,
          taskTester: newTaskTester,
          type,
          testCheck: newTestCheck,
          taskComment: newTaskComment,
        }

        setCompletedTasks(newCompletedTasks)
        localStorage.setItem('completedTasks', JSON.stringify(newCompletedTasks))
        setTaskEdit(false)
        setFlagAlarmError(false)
      }
    }
  }

  const testCheckStart = (stage:number) => {
    if (stage === 1) {
      if (newTestCheck === 0 || newTestCheck === 2 || newTestCheck === 3) {
        setNewTestCheck(1)
      } else if (newTestCheck === 1) {
        setNewTestCheck(0)
      }
    } else if (stage === 2) {
      if (newTestCheck === 0 || newTestCheck === 1 || newTestCheck === 3) {
        setNewTestCheck(2)
      } else if (newTestCheck === 2) {
        setNewTestCheck(1)
      }
    } else if (stage === 3) {
      if (newTestCheck === 0 || newTestCheck === 1 || newTestCheck === 2) {
        setNewTestCheck(3)
      } else if (newTestCheck === 3) {
        setNewTestCheck(2)
      }
    }
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы не заполнили данные!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  const renderCheckTest = (flag:boolean) => (
    <div className={ styles.wrapper_checkbox }>
      <Checkbox
        isDisabled={ flag }
        isInvalid
        defaultChecked={ !!newTestCheck }
        onChange={ () => testCheckStart(1) }
      >
        Проверка совпадения интерфейса проверяемой функции техническому заданию
      </Checkbox>
      <Checkbox
        isDisabled={ flag }
        isInvalid
        defaultChecked={ newTestCheck >= 2 }
        onChange={ () => testCheckStart(2) }
      >
        Проверка функциональных характеристик функции на основе технического задания
      </Checkbox>
      <Checkbox
        isDisabled={ flag }
        isInvalid
        defaultChecked={ newTestCheck === 3 }
        onChange={ () => testCheckStart(3) }
      >
        Анализ полученных результатов
      </Checkbox>
    </div>
  )

  const renderButton = () => {
    if (type === 1) {
      return (
        <div>
          <Button colorScheme="blue" onClick={ () => handleTaskWork() }>Перевести в работу</Button>
          <Button className={ styles.btn_edit } colorScheme="blue" onClick={ () => setTaskEdit(true) }>Редактировать</Button>
          {/* <button>Задача готова</button> */}
        </div>
      )
    }
    if (type === 2) {
      return (
        <div>
          {/* <button>Вернуть в созданные</button> */}
          <Button colorScheme="blue" onClick={ () => handleCompletedTasks() }>Задача готова</Button>
          <Button className={ styles.btn_edit } colorScheme="blue" onClick={ () => setTaskEdit(true) }>Редактировать</Button>
        </div>
      )
    }
    if (type === 3) {
      return (
        <div>
          {/* <Button colorScheme="blue">Вернуть в созданные</Button> */}
          {/* <Button colorScheme="blue">Вернуть в работу</Button> */}
          <Button className={ styles.btn_edit } colorScheme="blue" onClick={ () => setTaskEdit(true) }>Редактировать</Button>
        </div>
      )
    }
    return null
  }

  if (taskEdit) {
    return (
      <div>
        <section className={ styles.wrapper_move_page }>
          <div className={ styles.wrapper_content }>
            <Input
              value={ newTaskName }
              onChange={ e => setNewTaskName(e.target.value) }
            />
            <Textarea
              value={ newTaskDescription }
              onChange={ e => setNewTaskDescription(e.target.value) }
            />
            <p>
              Тестировщик:&ensp;
            </p>
            <Select placeholder="-" onChange={ e => setNewTaskTester(e.target.value) }>
              { testers.map(el => (
                <option key={ el.id } value={ el.name }>{el.name}</option>
              ))}
            </Select>
            <p>
              Статус:&ensp;
              {status}
            </p>
            <p>
              Дата создания:&ensp;
              {dateString}
            </p>
            { renderCheckTest(false) }
            <strong>Комментарий к задаче:</strong>
            <Textarea
              value={ newTaskComment }
              onChange={ e => setNewTaskComment(e.target.value) }
            />
            { alarmError() }
            <Button className={ styles.btn_edit } colorScheme="blue" onClick={ () => setTaskEditSave() }>Сохранить</Button>
          </div>
        </section>
      </div>
    )
  }

  return (
    <div>
      <section className={ styles.wrapper_move_page }>
        <div className={ styles.wrapper_content }>
          <strong className={ styles.title }>{newTaskName}</strong>
          <p>{newTaskDescription}</p>
          <p>
            Тестировщик:&ensp;
            {newTaskTester}
          </p>
          <p>
            Статус:&ensp;
            {status}
          </p>
          <p>
            Дата создания:&ensp;
            {dateString}
          </p>
          { renderCheckTest(true) }
          <p>Комментарий к задаче:</p>
          <p>{newTaskComment}</p>
          { renderButton() }
        </div>
      </section>
    </div>
  )
})

export default TaskPage;
