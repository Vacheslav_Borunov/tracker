import React, { FC, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles/index.module.css'
import Card from '../../components/Card';

interface Props {
  createdTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  workTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  completedTasks:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[],
  setСreatedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setWorkTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setCompletedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  setTaskInfo: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }) => void,
}

const Home: FC<Props> = ({
  createdTasks,
  workTasks,
  completedTasks,
  setTaskInfo,
  setСreatedTasks,
  setWorkTasks,
  setCompletedTasks,
}) => {
  const renderCreatedTasks = () => {
    if (createdTasks.length !== 0) {
      return (
        createdTasks.map(el => (
          <Card
            key={ el.time }
            time={ el.time }
            taskName={ el.taskName }
            taskDescription={ el.taskDescription }
            taskTester={ el.taskTester }
            type={ el.type }
            testCheck={ el.testCheck }
            taskComment={ el.taskComment }
            setTaskInfo={ setTaskInfo }
            createdTasks={ createdTasks }
            setСreatedTasks={ setСreatedTasks }
            workTasks={ workTasks }
            setWorkTasks={ setWorkTasks }
            completedTasks={ completedTasks }
            setCompletedTasks={ setCompletedTasks }
          />
        ))
      )
    }
    return null
  }

  const renderWorkTasks = () => {
    if (workTasks.length !== 0) {
      return (
        workTasks.map(el => (
          <Card
            key={ el.time }
            time={ el.time }
            taskName={ el.taskName }
            taskDescription={ el.taskDescription }
            taskTester={ el.taskTester }
            type={ el.type }
            testCheck={ el.testCheck }
            taskComment={ el.taskComment }
            setTaskInfo={ setTaskInfo }
            createdTasks={ createdTasks }
            setСreatedTasks={ setСreatedTasks }
            workTasks={ workTasks }
            setWorkTasks={ setWorkTasks }
            completedTasks={ completedTasks }
            setCompletedTasks={ setCompletedTasks }
          />
        ))
      )
    }
    return null
  }

  const renderCompletedTasks = () => {
    if (completedTasks.length !== 0) {
      return (
        completedTasks.map(el => (
          <Card
            key={ el.time }
            time={ el.time }
            taskName={ el.taskName }
            taskDescription={ el.taskDescription }
            taskTester={ el.taskTester }
            type={ el.type }
            testCheck={ el.testCheck }
            taskComment={ el.taskComment }
            setTaskInfo={ setTaskInfo }
            createdTasks={ createdTasks }
            setСreatedTasks={ setСreatedTasks }
            workTasks={ workTasks }
            setWorkTasks={ setWorkTasks }
            completedTasks={ completedTasks }
            setCompletedTasks={ setCompletedTasks }
          />
        ))
      )
    }
    return null
  }

  return (
    <div className={ styles.wrapper_all }>
      <div className={ styles.wrapper_title_app }>
        <p className={ styles.title_all_move }>Созданные</p>
        <p className={ styles.title_all_move }>В работе</p>
        <p className={ styles.title_all_move }>Готовые</p>
      </div>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_posters }>
          <div className={ styles.wrapper_card }>
            {renderCreatedTasks()}
          </div>
        </div>
        <div className={ styles.wrapper_posters }>
          <div className={ styles.wrapper_card }>
            {renderWorkTasks()}
          </div>
        </div>
        <div className={ styles.wrapper_posters }>
          <div className={ styles.wrapper_card }>
            {renderCompletedTasks()}
          </div>
        </div>
      </section>
    </div>
  );
}

export default Home;
