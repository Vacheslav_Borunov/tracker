import React, { FC, useEffect, useState } from 'react';
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button, Input, Select, Textarea,
} from '@chakra-ui/react'

import styles from './styles/index.module.css'

const LABELS = {
  CREATE_TASK: 'Создание задачи',
}

interface Props {
  setСreatedTasks: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  testers:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberTester: number,
    post: string,
  }[],
  createdTasks:[]
}

const CreateTask: FC<Props> = ({ setСreatedTasks, createdTasks, testers }) => {
  const [ taskName, setTaskName ] = useState<string>('')
  const [ taskDescription, setTaskDescription ] = useState<string>('')
  const [ taskTester, setTaskTester ] = useState<string>('')
  const [ flagAlarmError, setFlagAlarmError ] = useState<boolean>(false)

  const hendaleCreateTask = () => {
    if (!taskName || !taskDescription) {
      setFlagAlarmError(true)
    } else {
      const date = new Date().getTime()
      const type = 1
      const testCheck = 0
      const taskComment = ''

      setСreatedTasks([ ...createdTasks, {
        time: date, taskName, taskDescription, taskTester, type, testCheck, taskComment,
      }])
      localStorage.setItem('createdTasks', JSON.stringify([ ...createdTasks, {
        time: date, taskName, taskDescription, taskTester, type, testCheck, taskComment,
      }]))
      setTaskName('')
      setTaskDescription('')
      setFlagAlarmError(false)
    }
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы не заполнили данные!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  return (
    <div>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_posters }>
          <p className={ styles.wrapper_title }>{LABELS.CREATE_TASK}</p>
          <div className={ styles.wrapper_content }>
            <p>Название</p>
            <Input
              placeholder="Название"
              value={ taskName }
              onChange={ e => setTaskName(e.target.value) }
            />
            <p>Описание</p>
            <Textarea
              placeholder="Описание"
              value={ taskDescription }
              onChange={ e => setTaskDescription(e.target.value) }
            />
            <p>Тестировщик</p>
            <Select placeholder="-" onChange={ e => setTaskTester(e.target.value) }>
              { testers.map(el => (
                <option key={ el.id } value={ el.name }>{el.name}</option>
              ))}
            </Select>
            { alarmError() }
            <Button colorScheme="blue" onClick={ () => hendaleCreateTask() }>
              Создать задачу
            </Button>
          </div>
        </div>
      </section>
    </div>
  );
}

export default CreateTask;
