/* eslint-disable react/no-children-prop */
// @ts-nocheck
import React, { FC, useEffect, useState } from 'react';

import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button, Input, InputGroup, InputLeftAddon, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Radio, RadioGroup, Stack,
} from '@chakra-ui/react';
import iconTester from '../../image/icon_tester.png'
import styles from './styles/index.module.css'

interface Props {
  managers:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberManager: number,
    post: string,
  }[],
  setManagers: (params:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberManager: number,
    post: string,
  }[]) => void,
}

const Managers: FC<Props> = ({
  managers,
  setManagers,
}) => {
  const [ name, setName ] = useState<string>('')
  const [ floor, setFloor ] = useState<string>('')
  const [ age, setAge ] = useState<number>()
  const [ numberManager, setNumberManager ] = useState<number>()
  const [ post, setPost ] = useState<string>('')
  const [ flagAlarmError, setFlagAlarmError ] = useState<boolean>(false)

  const hendaleCreateManagers = () => {
    if (!name || !floor || !age || !numberManager || !post) {
      setFlagAlarmError(true)
    } else {
      const id = new Date().getTime()

      setManagers([ ...managers, {
        id, name, floor, age, numberManager, post,
      }])
      localStorage.setItem('managers', JSON.stringify([ ...managers, {
        id, name, floor, age, numberManager, post,
      }]))
      setName('')
      setFloor('')
      setAge(0)
      setNumberManager(0)
      setPost('')
      setFlagAlarmError(false)
    }
  }

  const deleteCard = id => {
    const newManagers = managers.filter(e => e.id !== id)

    setManagers(newManagers)
    localStorage.setItem('managers', JSON.stringify(newManagers))
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы не заполнили данные!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  const renderManagers = () => {
    if (managers.length !== 0) {
      return (
        managers.map(el => (
          <div className={ styles.wrapper_card_managers } key={ el.id }>
            <img alt="user_logo" src={ iconTester } className={ styles.wrapper_user_logo } />
            <div className={ styles.fio_info }>
              <p>
                ФИО:&ensp;
                {el.name}
              </p>
              <p>
                Пол:&ensp;
                {el.floor}
              </p>
              <p>
                Возраст:&ensp;
                {el.age}
              </p>
            </div>
            <div className={ styles.number_info }>
              <p>
                Номер:&ensp;
                {el.numberManager}
              </p>
              <p>
                Должность:&ensp;
                {el.post}
              </p>
            </div>
            <div className={ styles.button_edit }>
              <button onClick={ () => deleteCard(el.id) }>&#10060;</button>
            </div>
          </div>
        ))
      )
    }
    return null
  }

  return (
    <div>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_add_managers }>
          <strong>Добавление нового руководителя</strong>
          <p>ФИО</p>
          <Input
            value={ name }
            onChange={ e => setName(e.target.value) }
          />
          <p>Пол</p>
          <RadioGroup value={ floor } onChange={ setFloor }>
            <Stack direction="row">
              <Radio value="Мужской">Мужской</Radio>
              <Radio value="Женский">Женский</Radio>
            </Stack>
          </RadioGroup>
          <p>Возраст</p>
          <NumberInput onChange={ setAge } min={ 0 } max={ 150 }>
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <p>Номер телефона</p>
          <NumberInput onChange={ setNumberManager }>
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <p>Должность</p>
          <Input
            value={ post }
            onChange={ e => setPost(e.target.value) }
          />
          { alarmError() }
          <Button colorScheme="blue" onClick={ () => hendaleCreateManagers() }>
            Добавить руководителя
          </Button>
        </div>
        <div className={ styles.wrapper_managers }>
          <strong>Руководители</strong>
          {renderManagers()}
        </div>
      </section>
    </div>
  );
}

export default Managers;
