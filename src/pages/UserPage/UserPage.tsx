import React, { FC, useEffect, useState } from 'react';
import { Button } from '@chakra-ui/react';
import iconTester from '../../image/icon_tester.png'

import styles from './styles/index.module.css'

const LABELS = {
  TITLE_USER_PAGE: 'Личная страница',
  NAME_USER: 'Кузнецова Полина Сергеевна',
  GEN_USER: 'Жен',
  AGE_USER: '22 года',
  NUMBER: '89648750077',
  POST: 'Тестировщик',
}

const UserPage: FC = () => (
  <div>
    <section className={ styles.wrapper_usesr_page }>
      <div className={ styles.wrapper_user_card }>
        <div className={ styles.wrapper_user_info }>
          <img alt="user_logo" src={ iconTester } className={ styles.wrapper_user_logo } />
          <div className={ styles.fio_info }>
            <p>
              ФИО:&ensp;
              {LABELS.NAME_USER}
            </p>
            <p>
              Пол:&ensp;
              {LABELS.GEN_USER}
            </p>
            <p>
              Возраст:&ensp;
              {LABELS.AGE_USER}
            </p>
          </div>
          <div className={ styles.number_info }>
            <p>
              Номер:&ensp;
              {LABELS.NUMBER}
            </p>
            <p>
              Должность:&ensp;
              {LABELS.POST}
            </p>
            <Button className={ styles.btn_exit } size="sm" colorScheme="red">Выход</Button>
          </div>
        </div>
      </div>
    </section>
  </div>
)

export default UserPage;
