import React, { FC } from 'react';
import PropTypes from 'prop-types';
import styles from './styles/index.module.css'
import frontDropped from '../../image/front_dropped.png'

const NotfoundPage: FC = () => (
  <div>
    <section className={ styles.wrapper_list_error }>
      <div className={ styles.wrapper_error }>
        <strong className={ styles.title_front_dropped }>404</strong>
        <strong className={ styles.title_front_dropped }>Страница не найдена!</strong>
        <img alt="frontDropped" src={ frontDropped } className={ styles.front_dropped } />
      </div>
    </section>
  </div>
)

export default NotfoundPage;
