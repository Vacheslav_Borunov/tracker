// @ts-nocheck
import React, { FC, useEffect, useState } from 'react';

import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button, Input, InputGroup, InputLeftAddon, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Radio, RadioGroup, Stack,
} from '@chakra-ui/react';
import iconTester from '../../image/icon_tester.png'
import styles from './styles/index.module.css'

interface Props {
  testers:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberTester: number,
    post: string,
  }[],
  setTesters: (params:{
    id: number,
    name: string,
    floor: string,
    age: number,
    numberTester: number,
    post: string,
  }[]) => void,
}

const Testers: FC<Props> = ({
  testers,
  setTesters,
}) => {
  const [ name, setName ] = useState<string>('')
  const [ floor, setFloor ] = useState<string>('')
  const [ age, setAge ] = useState<number>()
  const [ numberTester, setNumberTester ] = useState<number>()
  const [ post, setPost ] = useState<string>('')
  const [ flagAlarmError, setFlagAlarmError ] = useState<boolean>(false)

  const hendaleCreateTesters = () => {
    if (!name || !floor || !age || !numberTester || !post) {
      setFlagAlarmError(true)
    } else {
      const id = new Date().getTime()

      setTesters([ ...testers, {
        id, name, floor, age, numberTester, post,
      }])
      localStorage.setItem('testers', JSON.stringify([ ...testers, {
        id, name, floor, age, numberTester, post,
      }]))
      setName('')
      setFloor('')
      setAge(0)
      setNumberTester(0)
      setPost('')
      setFlagAlarmError(false)
    }
  }

  const deleteCard = id => {
    const newTesters = testers.filter(e => e.id !== id)

    setTesters(newTesters)
    localStorage.setItem('testers', JSON.stringify(newTesters))
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы не заполнили данные!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  const renderTesters = () => {
    if (testers.length !== 0) {
      return (
        testers.map(el => (
          <div className={ styles.wrapper_card_tester } key={ el.id }>
            <img alt="user_logo" src={ iconTester } className={ styles.wrapper_user_logo } />
            <div className={ styles.fio_info }>
              <p>
                ФИО:&ensp;
                {el.name}
              </p>
              <p>
                Пол:&ensp;
                {el.floor}
              </p>
              <p>
                Возраст:&ensp;
                {el.age}
              </p>
            </div>
            <div className={ styles.number_info }>
              <p>
                Номер:&ensp;
                {el.numberTester}
              </p>
              <p>
                Должность:&ensp;
                {el.post}
              </p>
            </div>
            <div className={ styles.button_edit }>
              <button onClick={ () => deleteCard(el.id) }>&#10060;</button>
            </div>
          </div>
        ))
      )
    }
    return null
  }

  return (
    <div>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_add_testers }>
          <strong>Добавление нового тестировщика</strong>
          <p>ФИО</p>
          <Input
            value={ name }
            onChange={ e => setName(e.target.value) }
          />
          <p>Пол</p>
          <RadioGroup value={ floor } onChange={ setFloor }>
            <Stack direction="row">
              <Radio value="Мужской">Мужской</Radio>
              <Radio value="Женский">Женский</Radio>
            </Stack>
          </RadioGroup>
          <p>Возраст</p>
          <NumberInput onChange={ setAge } min={ 0 } max={ 150 }>
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <p>Номер телефона</p>
          <NumberInput onChange={ setNumberTester }>
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <p>Должность</p>
          <Input
            value={ post }
            onChange={ e => setPost(e.target.value) }
          />
          { alarmError() }
          <Button colorScheme="blue" onClick={ () => hendaleCreateTesters() }>
            Добавить тестировщика
          </Button>
        </div>
        <div className={ styles.wrapper_testers }>
          <strong>Тестировщики</strong>
          {renderTesters()}
        </div>
      </section>
    </div>
  );
}

export default Testers;
